
$%function rewrite_shebang(builtscr) {
    sed -r -e '1 s|^#!.+bbrun$|#!/usr/bin/env bash|g' -i "$builtscr"

    debug:print "Shebang: $(head -n 1 "$builtscr")"
}

