# Bash Builder Demos

A few demo files to show how much easier bash scripting is with Bash Builder

## CronDo

Simple example using some libraries to build a complex script.

## cdiff

Simple example demonstrating use of syntax extensions.

## SSH Connection Manager

More complex example tool for managing a list of connections, and the use of multiple source files.

## MyIP

Example of a project that uses TarSH asset bundling.
