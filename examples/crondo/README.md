CronDo
=======
 
Run cron job line immediately.
 
Add `:;` to the start of any line which you want to access via `crondo.sh`
 
For example, if you have the following in your crontab:
 
  	* * * * * echo Hello
  	* * * * * :; echo Goodbye
 
You will only be offered the option of running the second command.
 
(`:` is a command that does nothing; normally used as a placeholder.
Check that this is the case on your system (that is has not been overridden by a command or alias: issuing the command `type :` should return `: is a shell builtin` ))
 
